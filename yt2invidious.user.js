// ==UserScript==
// @name     yt2invidious
// @description Easily jump from yt to invidio.us.
// @updateURL https://openuserjs.org/meta/marcowahl/yt2invidious.meta.js
// @downloadURL https://openuserjs.org/install/marcowahl/yt2invidious.user.js
// @copyright 2019,2020, marcowahl (https://openuserjs.org/users/marcowahl)
// @version  1.1.2
// @include *youtube.com*
// @grant    none
// @author   Marco Wahl
// @namespace      https://gitlab.com/marcowahl
// @licence GPL-3.0-or-later; http://www.gnu.org/licenses/gpl-3.0.txt
// @homepageURL https://gitlab.com/marcowahl
// ==/UserScript==

function invidio_url() {
    const this_url = window.location.href
    const index_of_watch = this_url.indexOf("watch")
    let watch_spec = this_url.slice(index_of_watch)
    const index_of_ampersand = watch_spec.indexOf("&")
    if (-1 < index_of_ampersand)
        watch_spec = watch_spec.slice(0, index_of_ampersand)
    return "https://invidio.us/" + watch_spec
}

const element = document.createElement("button")
element.innerHTML = "invidious"
element.style.left = "0px"
element.style.top = "0px"
element.style.position = "fixed"
element.style.background = "orange"
element.style.zIndex = 99999
element.onclick = function () {
    window.location = invidio_url()
}
document.body.appendChild(element)


document.onfullscreenchange = function (event) {
    if (document.fullscreenElement) {
        document.body.removeChild(element)
    } else {
        document.body.appendChild(element)
    }
};

// {{{ changelog :

// [2020-02-07 Fri] 1.1.2 Fix cut off further parameters in the url.

// [2020-01-12 Sun] 1.1.1 Rollback to button since the link remains
// when switching to other videos.  I.e. the link does not follow the
// current video.

// [2020-01-12 Sun] 1.1.0 A simple link leads to invidious.  This
// replaces the former button.  This allows using the "open in new
// tab" feature.

// }}}